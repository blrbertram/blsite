import { Service } from '../_models/service.interface';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../_models/hero.interface';
import { DataService } from '../_services/data.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  private heros?: Hero[];
  private services: Service[];
  servicesDisplayed: Service[] = [];
  hero?: Hero = {};

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.getHero();
    this.getServices();
  }

  getHero() {
    this.ds.getHeros().subscribe(gotHeros => {
      this.heros = gotHeros;
    }, err => {}, () => {
      const ranNum = Math.floor(Math.random() * this.heros.length);
      this.hero = this.heros[ranNum];
    });
  }

  getServices() {
    this.ds.getServices().subscribe(gotServices => {
      this.services = gotServices;
    }, err => {}, () => {
      while (this.servicesDisplayed.length < 3) {
        const ranNum = Math.floor(Math.random() * this.services.length);
        if (!this.servicesDisplayed.includes(this.services[ranNum])) {
           this.servicesDisplayed.push(this.services[ranNum]);
        }
      }
    });
  }

}
