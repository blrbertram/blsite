import { Appointment } from '../_models/appointment.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Hero } from '../_models/hero.interface';
import { environment } from '../../environments/environment.prod';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Service } from '../_models/service.interface';
import { TeamMember } from '../_models/teammember.interface';
import { Technology } from '../_models/technology.interface';
import { About } from '../_models/about.interface';


@Injectable()
export class DataService {
    baseUrl = environment.baseUrl;
    railsUrl = environment.railsUrl;

    constructor(private http: HttpClient) {}

  getHeros(): Observable<Hero[]> {
      return this.http.get(this.baseUrl + 'hero/')
                  .map(response => <Hero[]>response);
  }

  getServices(): Observable<Service[]> {
    return this.http.get(this.baseUrl + 'services/')
               .map(response => <Service[]>response);
  }

  getTeamMembers(): Observable<TeamMember[]> {
    return this.http.get(this.baseUrl + 'teammembers/')
              .map(response => <TeamMember[]>response);
  }

  getAbout(): Observable<About> {
    // return this.http.get('http://localhost:3000/rails_api/about/')
    return this.http.get(this.railsUrl + 'about/')
              .map(response => <About>response);
  }

  getTechnologies(): Observable<Technology[]> {
    // return this.http.get('http://localhost:3000/rails_api/technology/')
    return this.http.get(this.railsUrl + 'technology/')
        .map(response => <Technology[]>response)
  }

  createAppointment(appointment: Appointment) {
    return this.http.post(this.baseUrl + 'appointment/', appointment);
  }

}
