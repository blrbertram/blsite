import { DataService } from '../_services/data.service';
import { Component, OnInit } from '@angular/core';
import { Service } from '../_models/service.interface';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})

export class ServicesComponent implements OnInit {
  services: Service[];

  constructor(private ds: DataService) {}

  ngOnInit() {
    this.getServices();
  }

  getServices() {
    this.ds.getServices().subscribe(gotServices => {
      this.services = gotServices;
    });
  }
}
