import { DataService } from './../_services/data.service';
import { Appointment } from './../_models/appointment.interface';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

import * as moment from 'moment';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  appointmentTime: any;
  start = moment(Date.now()).toDate();
  thankYou = false;
  postError = false;
  name: string;
  phone: string;
  time: string;
  appointmentSubsciption: Subscription;

  constructor(private ds: DataService) { }

  ngOnInit() {}


  onSubmit(form: NgForm) {
    if (form.valid) {
          const appointmentMade: Appointment = {
              contact: {
                person: form.value['name'],
                company: form.value['organization'] ? form.value['organization'] : 'n/a',
                phone: form.value['phone'],
                email: form.value['email'],
              },
              appointment: form.value['dateTimeInput']
        };
        this.appointmentSubsciption = this.ds.createAppointment(appointmentMade).subscribe(() => {
          this.name = form.value['name'];
          this.phone = form.value['phone'];
          this.time = moment(form.value['dateTimeInput']).toLocaleString();
        }, err => {
          this.postError = true;
        }, () => {
          this.thankYou = true;
        });
    }

  }

  // ngOnDestroy() {
  //   this.appointmentSubsciption.unsubscribe();
  // }

}
