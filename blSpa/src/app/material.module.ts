import { NgModule } from '@angular/core';
import { MatFormFieldModule,
         MatIconModule,
         MatButtonModule,
         MatInputModule,
         MatDatepickerModule,
         MatNativeDateModule
         } from '@angular/material';

@NgModule({
    imports: [
      MatFormFieldModule,
      MatIconModule,
      MatButtonModule,
      MatInputModule,
      MatDatepickerModule,
      MatNativeDateModule
    ],
    exports: [
      MatFormFieldModule,
      MatIconModule,
      MatButtonModule,
      MatInputModule,
      MatDatepickerModule,
      MatNativeDateModule
    ]
})

export class MaterialModule {}
