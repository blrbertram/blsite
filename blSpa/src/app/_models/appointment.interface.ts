import { Contact } from './contact.interface';

export interface Appointment {
  contact?: Contact;
  appointment?: Date;
  google_appointment_id?: string;
}
