export interface Technology {
    name?: string;
    description?: string;
    image?: string;
}
