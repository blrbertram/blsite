export interface Contact {
  person?: string;
  company?: string;
  phone?: string;
  email?: string;
}
