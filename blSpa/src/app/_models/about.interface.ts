export interface About {
  headline?: string;
  article?: string;
}
