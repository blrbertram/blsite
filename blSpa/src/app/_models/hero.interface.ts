export interface Hero {
    name?: string;
    description?: string;
    background_url?: string;
    cover_color?: string;
    headline?: string;
    tagline?: string;
}
