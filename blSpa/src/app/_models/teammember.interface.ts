export interface TeamMember {
    name: string;
    email: string;
    role: string;
    specialties: string;
    hobbies: string;
    photo: string;
}
