export interface Hours {
  day_of_week?: string;
  start_time?: string;
  end_time?: string;
}
