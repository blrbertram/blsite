export interface Service {
    name?: string;
    description?: string;
    headline?: string;
    tagline?: string;
}
