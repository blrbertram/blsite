import { Component, OnInit } from '@angular/core';
import { About } from '../_models/about.interface';
import { DataService } from '../_services/data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  private data: About;
  about: About = {};
  logo?: String;

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.getAbout();
  }

  getAbout() {
    this.ds.getAbout().subscribe(d => { this.about = d },
      () => {}, () => {
        let asset_path = '';
        let imagepath = ['blwebtech_hexmesh_logo001.png', 'blwebtech_hexmesh_logo002.png', 'favicon_logo_1024x1024_show.png'];
        const rnd = Math.floor(Math.random() * imagepath.length);
        this.logo = 'https://www.blwebtech.com/assets/' + imagepath[rnd];
        console.log("logo: ", this.logo);
      }
      );
  }
}
