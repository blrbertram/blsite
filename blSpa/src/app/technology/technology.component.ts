import { Component, OnInit } from '@angular/core';
import { Technology } from '../_models/technology.interface';
import { DataService } from '../_services/data.service';

@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.scss']
})
export class TechnologyComponent implements OnInit {
  private tech_data: Technology[];
  technologies: Technology[] = [];

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.getTechnologies();
  }

  getTechnologies() {
    let i, t, l;
    this.ds.getTechnologies().subscribe(techlist => {
      this.tech_data = techlist;
    }, err => {}, () => {
      l = this.tech_data.length;
      for (i=0; i<l; i++) {
        let t = this.tech_data[i];
        t['side'] = 'imgright';
        if ((i%2) == 0)
          t['side'] = 'imgleft';


        this.technologies.push(t)
      }
    });
  }

}
