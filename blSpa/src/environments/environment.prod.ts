export const environment = {
  production: true,
  baseUrl: 'https://www.blwebtech.com/api/',
  railsUrl: 'https://www.blwebtech.com/rails_api/'
};
