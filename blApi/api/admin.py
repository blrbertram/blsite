from django.contrib import admin

from .models import (About, Hero, Hours, SocialMedia,
                     Address, Contact, Service, Appointment,
                     TeamMembers, Technology)


admin.site.register([About, Hero, Hours, SocialMedia,
                     Address, Contact, Service, Appointment,
                     TeamMembers, Technology])
