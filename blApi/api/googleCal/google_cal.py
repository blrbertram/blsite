import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

from datetime import datetime, timedelta
import iso8601


SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = './api/googleCal/client_secret.json'
APPLICATION_NAME = 'DataPoint GIS Appointment Maker'

class MakeAppointment():


    def get_credentials(self):
        cred_dir = os.path.expanduser('./api/googleCal/')
        credential_dir = os.path.join(cred_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                    'calendar-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        return credentials


    def create_event(self, appointment):
        return {'summary': 'Initial Phone Consultation with BL WebTech',
                'location': 'Phone',
                'description': f'Phone call {appointment.name} at phone number {appointment.phone}',
                'start': {'dateTime': appointment.time.isoformat()},
                'end': {'dateTime': (appointment.time + timedelta(seconds=1800)).isoformat()},
                'attendees': [
                    {'email': appointment.email}
                ],
                'reminders': {
                        'useDefault': False,
                        'overrides': [
                        {'method': 'email', 'minutes': 24 * 60},
                        {'method': 'popup', 'minutes': 10},
                        ],
                    },
        }


    def save_event(self, appointment):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('calendar', 'v3', http=http)
        event = self.create_event(appointment)
        event = service.events().insert(calendarId='primary', body=event).execute()
        return event['id']
