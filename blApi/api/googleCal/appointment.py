from dateutil import parser

class AppointmentData:
    def __init__(self, name, company, phone, email, time):
        self.name = name
        self.company = company
        self.phone = phone
        self.email = email
        self.time = parser.parse(time)

        