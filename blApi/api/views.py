from .serializers import (AppointmentSerializer, AboutSerializer, HeroSerializer,
                          HoursSerializer, SocialMediaSerializer,
                          AddressSerializer, ServiceSerializer, TeamMembersSerializer,
                          TechnologySerializer)
from .models import (Appointment, About, Hero, Hours,
                     SocialMedia, Address, Service,
                     TeamMembers, Technology)
from rest_framework import mixins
from rest_framework import generics
from .googleCal.appointment import AppointmentData
from .googleCal.google_cal import MakeAppointment


class CreateAppointment(mixins.CreateModelMixin,
                        generics.GenericAPIView):

    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer

    def post(self, request, *args, **kwargs):
        appointment_contact = request.data
        contact = appointment_contact['contact']
        appointment = AppointmentData(contact['person'],
                                  contact['company'],
                                  contact['phone'],
                                  contact['email'],
                                  appointment_contact['appointment'])
        make_appointment = MakeAppointment()
        googleApp = make_appointment.save_event(appointment)
        request.data['google_appointment_id'] = googleApp
        return self.create(request, *args, **kwargs)


class AboutList(generics.ListAPIView):
    queryset = About.objects.all()
    serializer_class = AboutSerializer

class HeroList(generics.ListAPIView):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer

class HoursList(generics.ListAPIView):
    queryset = Hours.objects.all()
    serializer_class = HoursSerializer

class SocialMediaList(generics.ListAPIView):
    queryset = SocialMedia.objects.all()
    serializer_class = SocialMediaSerializer

class AddressList(generics.ListAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

class ServicesList(generics.ListAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

class TeamMembersList(generics.ListAPIView):
    queryset = TeamMembers.objects.all()
    serializer_class = TeamMembersSerializer

class TechnologyList(generics.ListAPIView):
    queryset = Technology.objects.all()
    serializer_class = TechnologySerializer
