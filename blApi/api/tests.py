from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from django.utils.timezone import datetime
from .models import (About, Hero, Service, Contact,
                     SocialMedia, Hours, Address, Appointment)

class AboutTestCase(TestCase):
    """ This class defines the test suite for the about models """
    def setUp(self):
        self.about_headline = "This is the greatest development firm ever!"
        self.article = "Have you ever felt you could be getting more out of your software"
        self.about = About(headline=self.about_headline, article=self.article)
        
    def test_model_can_create_an_about(self):
        old_count = About.objects.count()
        self.about.save()
        new_count = About.objects.count()
        self.assertNotEqual(old_count, new_count)
        
    
class HeroTestCase(TestCase):

    def setUp(self):
        self.name = "Test Hero"
        self.description = "This is just a test"
        self.background_url = SimpleUploadedFile(name='test_image.jpg', 
                                                 content=open('test_image.jpg', 'rb').read(),
                                                 content_type='image/jpeg')
        self.cover_color = "#0000FF"
        self.headline = "We Serve West Michigan's Web Technology Needs"
        self.tagline = "We Serve The Best"
        self.hero = Hero(name=self.name, description=self.description, background_url=self.background_url,
                         cover_color=self.cover_color, headline=self.headline, tagline=self.tagline)
        
    
    def test_model_can_create_a_hero(self):
        old_count = Hero.objects.count()
        self.hero.save()
        new_count = Hero.objects.count()
        self.assertNotEqual(old_count, new_count)


class HoursTestCase(TestCase):
    
    def setUp(self):
        self.day_of_week = "Monday"
        self.start_time = "9am"
        self.close_time = "6pm"
        self.hours = Hours(day_of_week=self.day_of_week, start_time=self.start_time, close_time=self.close_time)

    def test_model_can_create_hours(self):
        old_count = Hours.objects.count()
        self.hours.save()
        new_count = Hours.objects.count()
        self.assertNotEqual(old_count, new_count)




class SocialMediaTestCase(TestCase):
    
    def setUp(self):
        self.name = "Facebook"
        self.url = "https://www.facebook.com"
        self.social_media = SocialMedia(name=self.name, url=self.url)

    def test_model_can_create_social_media(self):
        old_count = SocialMedia.objects.count()
        self.social_media.save()
        new_count = SocialMedia.objects.count()
        self.assertNotEqual(old_count, new_count)


class AddressTestCase(TestCase):
    
    def setUp(self):
        self.street1 = "898 Waltham Ave"
        self.street2 = "Suite 3124"
        self.city = "Holland"
        self.state = "Michigan"
        self.postal_code = "49423"
        self.address = Address(
            street1 = self.street1,
            street2 = self.street2,
            city = self.city,
            state_province = self.state,
            postal_code = self.postal_code
        )

    def test_model_can_create_social_media(self):
        old_count = Address.objects.count()
        self.address.save()
        new_count = Address.objects.count()
        self.assertNotEqual(old_count, new_count)

class ContactTestCase(TestCase):

    def setUp(self):
        self.person = "Douglas Jones"
        self.company = "Steelcase"
        self.phone = "989-444-2334"
        self.email = "hi@hello.com"

        self.contact = Contact(
            person = self.person,
            company = self.company,
            phone = self.phone,
            email = self.email,
        )

    def test_model_can_create_contact(self):
        old_count = Contact.objects.count()
        self.contact.save()
        new_count = Contact.objects.count()
        self.assertNotEqual(old_count, new_count)


class ServiceTestCase(TestCase):

    def setUp(self):
        self.name = "API development"
        self.description = "Server side application that provides a data to other apps"
        self.headline = "API Development"
        self.tagline = "Use Data Anywhere"
        self.service = Service(
            name = self.name,
            description = self.description,
            headline = self.headline,
            tagline = self.tagline
        )

    def test_model_can_create_service(self):
        old_count = Service.objects.count()
        self.service.save()
        new_count = Service.objects.count()
        self.assertNotEqual(old_count, new_count)

class AppointmentTestCase(TestCase):
    
    def setUp(self):
       
        self.contact = Contact.objects.create(
            person = "Douglas Dexter",
            company = "Google",
            phone = "555-1212",
            email = "hi@hello.com"
        )

        self.appointment = datetime.now()
        self.google_appointment_id = "48429042085092sjadfkjl;sdl;j230923"
        self.appointment = Appointment(
            contact = self.contact,
            appointment = self.appointment,
            google_appointment_id = self.google_appointment_id
        )

        def test_model_can_create_appointment(self):
            old_count = Appointment.objects.count()
            self.appointment.save()
            new_count = Appointment.objects.count()
            self.assertNotEqual(old_count, new_count)
    
#View test case.

class AppointmentViewTestCase(TestCase):
    
    def setUp(self):
        self.client = APIClient()
        self.appointment = {
            'contact': {
                'person': 'Bob Hope',
                'company': 'State of Michigan',
                'phone': '423-323-3223',
                'email': 'bob@nyc.gov',  
            },
            'appointment': '2018-04-05T13:00:08-04:00',
            'google_appointment_id': '3209384209sljsf3290329023'
        }

        self.response = self.client.post(
            reverse('create'),
            self.appointment,
            format="json"
        )

    def test_api_can_create_appointment(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

        
