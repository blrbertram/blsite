from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (CreateAppointment, AboutList, HeroList, HoursList,
                    SocialMediaList, AddressList, ServicesList,
                    TeamMembersList, TechnologyList)

urlpatterns = [
    path('appointment/', CreateAppointment.as_view(), name='create'),
    path('about/', AboutList.as_view(), name='about'),
    path('hero/', HeroList.as_view(), name='hero'),
    path('hours/', HoursList.as_view(), name='hours'),
    path('socialmedia/', SocialMediaList.as_view(), name='social_media'),
    path('address/', AddressList.as_view(), name='address'),
    path('services/', ServicesList.as_view(), name='services'),
    path('technology/', TechnologyList.as_view(), name='technology'),
    path('teammembers/', TeamMembersList.as_view(), name='teammembers'),
]


urlpatterns = format_suffix_patterns(urlpatterns)
