from django.db import models
from django.http import HttpResponse


class About(models.Model):
    headline = models.CharField(max_length=255, blank=False, unique=False)
    article = models.TextField(blank=False, unique=False)
    image = models.ImageField(upload_to='about/', blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "About"

    def __str__(self):
        return self.headline

class Hero(models.Model):
    name = models.CharField(max_length=255, blank=False, unique=False)
    description = models.TextField(blank=False, unique=False)
    background_url = models.ImageField(upload_to='hero/',max_length=255, blank=True)
    cover_color = models.CharField(max_length=10, blank=True, null=True)
    headline = models.CharField(max_length=255, blank=False, unique=False)
    tagline = models.CharField(max_length=255, blank=False, unique=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Hours(models.Model):
    day_of_week = models.CharField(max_length=9, blank=False, null=False)
    start_time = models.CharField(max_length=8, blank=False, null=False)
    close_time = models.CharField(max_length=8, blank=False, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Hours"
        verbose_name_plural = "Hours"
    def __str__(self):
        return self.day_of_week



class SocialMedia(models.Model):
    name = models.CharField(max_length=25, blank=False, null=False)
    url = models.URLField(blank=False, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.name



class Address(models.Model):
    street1 = models.CharField(max_length=255, blank=True, null=True)
    street2 = models.CharField(max_length=80, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state_province = models.CharField(max_length=45, blank=True, null=True)
    postal_code = models.CharField(max_length=25, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

    def __str__(self):
        return self.street1



class Contact(models.Model):
    person = models.CharField(max_length=50, blank=False, null=False)
    company = models.CharField(max_length=125, blank=False, null=True)
    phone = models.CharField(max_length=25, blank=True, null=True)
    email = models.EmailField(max_length=50, blank=False, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.person} {self.company}"


class Service(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    description = models.CharField(max_length=255, blank=False, null=False)
    headline = models.CharField(max_length=255, blank=False, unique=False)
    tagline = models.CharField(max_length=255, blank=False, unique=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Appointment(models.Model):
    contact = models.ForeignKey(Contact, blank=False, null=False, on_delete=models.CASCADE)
    appointment = models.DateTimeField(blank=False, null=False)
    google_appointment_id = models.CharField(max_length=200, null=False, blank=False)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.appointment

class TeamMembers(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    email = models.EmailField(max_length=50, blank=False, null=False)
    role = models.CharField(max_length=50, blank=False, null=False)
    specialties = models.CharField(max_length=255, blank=False, null=False)
    hobbies = models.CharField(max_length=255, blank=False, null=False)
    photo = models.ImageField(upload_to='team_member/')

    class Meta:
       verbose_name_plural = "Team Members"

    def __str__(self):
        return self.name

class Technology(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    description = models.TextField()
    image = models.ImageField(upload_to='tech/', blank=True, null=True)
    pos = models.IntegerField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
       verbose_name = "Technology"
       verbose_name_plural = "Technologies"
    

    def __str__(self):
       return self.name
