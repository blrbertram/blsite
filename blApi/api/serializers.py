from rest_framework import serializers
from .models import (About, Hero, Contact, Service,
                     Hours, SocialMedia, Address, Appointment,
                     TeamMembers, Technology)


class AboutSerializer(serializers.ModelSerializer):

    class Meta:
        model = About
        fields = ('id', 'headline', 'article','image', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class HeroSerializer(serializers.ModelSerializer):

    class Meta:
        model = Hero
        fields = ('name', 'description', 'background_url', 'cover_color',
                  'headline', 'tagline', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class TechnologySerializer(serializers.ModelSerializer):
    class Meta:
        model = Technology
        fields = ('name', 'description', 'image')
        read_only_fields = ('date_created', 'date_modified')


class HoursSerializer(serializers.ModelSerializer):

    class Meta:
        model = Hours
        fields = ('day_of_week', 'start_time', 'close_time')
        read_only_fields = ('date_created', 'date_modified')

class SocialMediaSerializer(serializers.ModelSerializer):

    class Meta:
        model = SocialMedia
        fields = ('name', 'url', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ('street1', 'street2', 'city', 'state_provinece'
                  'postal_code', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class ContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = ('person', 'company',  'phone', 'email', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class ServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = ('name', 'description', 'headline', 'tagline', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class AppointmentSerializer(serializers.ModelSerializer):
    contact = ContactSerializer()

    class Meta:
        model = Appointment
        fields = ('contact', 'appointment', 'google_appointment_id', 'date_created')
        read_only_fields = ('date_created',)

    def create(self, validated_data):
        contact_data = validated_data.pop('contact')
        contact_create = Contact.objects.create(**contact_data)
        appointment = Appointment.objects.create(contact=contact_create, **validated_data)
        return appointment

class TeamMembersSerializer(serializers.ModelSerializer):

   class Meta:
       model = TeamMembers
       fields = ('name', 'email', 'role', 'specialties', 'hobbies', 'photo')






